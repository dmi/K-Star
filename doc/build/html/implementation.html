
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Implementation &#8212; K*  documentation</title>
    <link rel="stylesheet" href="_static/alabaster.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    <script type="text/javascript" src="_static/documentation_options.js"></script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <div class="section" id="implementation">
<h1>Implementation<a class="headerlink" href="#implementation" title="Permalink to this headline">¶</a></h1>
<p>While the choice of the datastructure for the problem graph <span class="math notranslate nohighlight">\(G\)</span> is rather straight forward in this project, the crucial part is the implementation
of the path graph <span class="math notranslate nohighlight">\(PG\)</span> and the storage of <span class="math notranslate nohighlight">\(PG\)</span>-paths discovered in D-steps.</p>
<p>For the nodes of <span class="math notranslate nohighlight">\(G\)</span> there are node-objects, which hold basic infomation like name, coordinates for the heuristic and a predecessor for A-steps,
but also a list of all outgoing edges, while an edge is its own object with pointers to its origin- and target-nodes and a cost-value.
This is sufficient to run the A-step expansions.</p>
<p>The path graph on the other hand requires methods to quickly insert new nodes into the heap structure.
Here, we implement the incoming- and tree-heaps as simple arrays of pointers to <span class="math notranslate nohighlight">\(PG\)</span>-node objects.
These <span class="math notranslate nohighlight">\(PG\)</span>-node objects do not hold any pointers to child- or parent-nodes. They just contain the information about the edges they represent.
Arrays for incoming-heaps start with index 0 and arrays for tree-heaps start with index 1, where in slot 0 there is just a place holder.
For further indexing we use the <em>Sosa-Stradonitz System</em>, where the first child (if available) of the node with index <span class="math notranslate nohighlight">\(i\)</span> has index <span class="math notranslate nohighlight">\(2\mathrel{\cdot}i\)</span>
and the second child (if available) has index <span class="math notranslate nohighlight">\(2\mathrel{\cdot}i + 1\)</span>, while the parent (if available) has index <span class="math notranslate nohighlight">\(i\mathrel{/}2\)</span> if <span class="math notranslate nohighlight">\(i\)</span> is even
and <span class="math notranslate nohighlight">\((i - 1)\mathrel{/}2\)</span> if <span class="math notranslate nohighlight">\(i\)</span> is odd.
The cost-values of <span class="math notranslate nohighlight">\(PG\)</span>-edges are not stored anywhere. They are calculated in the D-steps or the maximum calculation in <em>K*</em>, whenever they are needed.</p>
<div class="figure align-center" id="id1">
<img alt="_images/Sosa-Stradonitz-System.svg" src="_images/Sosa-Stradonitz-System.svg" /><p class="caption"><span class="caption-text">The Sosa-Stradonitz indexing</span></p>
</div>
<p>The incoming-heaps of nodes in <span class="math notranslate nohighlight">\(G\)</span> are built as soon as the nodes are expanded by an A-step and kept up-to-date whenever a new sidetrack-edge is found during an A-step.
The path graph is updated after each A-step in a way that the tree-heaps of all already expanded nodes are built recursively from scratch.
A version number of the tree-heap is stored in each <span class="math notranslate nohighlight">\(G\)</span>-node-object to make sure that the recursive algorithm doesn’t rebuild already completed tree-heaps.</p>
<p>Because there are no pointers between the <span class="math notranslate nohighlight">\(PG\)</span>-nodes, the methods which use the heaps must include all index computations themselves.
Each object for nodes in <span class="math notranslate nohighlight">\(G\)</span> holds pointers to its tree- and incoming-heap arrays. These can be used to find the only incoming-heap-child of a <span class="math notranslate nohighlight">\(root_{in}\)</span>-node,
if the latter is found in a tree-heap:
First, one can look up the target-node of the represented sidetrack-edge and then one can consider the pointer at index 1 of this node’s incoming-heap array.
The child-nodes behind cross-edges can be found by looking at the origin of the sidetrack-edge represented by the considered <span class="math notranslate nohighlight">\(PG\)</span>-nodes.
After a <span class="math notranslate nohighlight">\(PG\)</span>-node is discovered by a D-step, it doesn’t change its position in the path-graph any more (cf. Lemma 7 in [AlLe11] - see <a href="#id2"><span class="problematic" id="id3">`Literature`_</span></a>).
This is why to store a reference to a <span class="math notranslate nohighlight">\(PG\)</span>-node, it is sufficient to store the type of the heap that it is included in,
the owning node of that heap and the index of the position in it.</p>
<p>To store <span class="math notranslate nohighlight">\(PG\)</span>-paths that have been followed during the performance of D-steps, chains of such storage-objects (named <em>CD_list_obj</em>-objects in the code) are used.
It is important to note the bijection between <span class="math notranslate nohighlight">\(PG\)</span>-paths starting at <span class="math notranslate nohighlight">\(\mathcal{R}\)</span> and sequences of successor-choices in <span class="math notranslate nohighlight">\(PG\)</span> made by D-steps.
These sequences of successor-choices are used to effectively store <span class="math notranslate nohighlight">\(PG\)</span>-paths <span class="math notranslate nohighlight">\(\sigma\)</span> starting at <span class="math notranslate nohighlight">\(\mathcal{R}\)</span> in the algorithm which of
<span class="math notranslate nohighlight">\(\textrm{seq}(\sigma)\)</span> and <span class="math notranslate nohighlight">\(\chi(\textrm{seq}(\sigma))\)</span> is computed later.
In practice, what is stored is not the sequence, but each entry in the open list (and also the closed list) for D-steps represents a successor-choice,
where the corresponding <em>CD_list_obj</em>-object contains a predecessor-pointer to a <em>CD_list_obj</em>-object on the closed list, which makes it possible to trace back
the complete <span class="math notranslate nohighlight">\(PG\)</span>-path taken.
The <em>CD_list_obj</em>-objects also hold a boolean storing whether the successor is behind a cross-edge or not, which is needed to compute <span class="math notranslate nohighlight">\(\textrm{seq}(\sigma)\)</span>.</p>
<p>Since in this project we are loading nodes and edges from files and not using databases etc. with the consequence that the edges-file must
be read completely to collect all outgoing edges per node, searching for nodes and edges on-the-fly is even less effective here than complete
preassembling of <span class="math notranslate nohighlight">\(G\)</span>. This is why we leave out this essential on-the-fly aspect of the original <em>K*</em> algorithm for the original graph <span class="math notranslate nohighlight">\(G\)</span>.</p>
</div>


          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper"><div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
  </ul></li>
</ul>
</div>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="_sources/implementation.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3>Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" />
      <input type="submit" value="Go" />
      <input type="hidden" name="check_keywords" value="yes" />
      <input type="hidden" name="area" value="default" />
    </form>
    </div>
</div>
<script type="text/javascript">$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2019, Daniel Missal.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 1.7.4</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.10</a>
      
      |
      <a href="_sources/implementation.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>