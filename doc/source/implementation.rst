Implementation
==============

While the choice of the datastructure for the problem graph :math:`G` is rather straight forward in this project, the crucial part is the implementation
of the path graph :math:`PG` and the storage of :math:`PG`-paths discovered in D-steps.

For the nodes of :math:`G` there are node-objects, which hold basic infomation like name, coordinates for the heuristic and a predecessor for A-steps,
but also a list of all outgoing edges, while an edge is its own object with pointers to its origin- and target-nodes and a cost-value.
This is sufficient to run the A-step expansions.

The path graph on the other hand requires methods to quickly insert new nodes into the heap structure.
Here, we implement the incoming- and tree-heaps as simple arrays of pointers to :math:`PG`-node objects.
These :math:`PG`-node objects do not hold any pointers to child- or parent-nodes. They just contain the information about the edges they represent.
Arrays for incoming-heaps start with index 0 and arrays for tree-heaps start with index 1, where in slot 0 there is just a place holder.
For further indexing we use the *Sosa-Stradonitz System*, where the first child (if available) of the node with index :math:`i` has index :math:`2\mathrel{\cdot}i`
and the second child (if available) has index :math:`2\mathrel{\cdot}i + 1`, while the parent (if available) has index :math:`i\mathrel{/}2` if :math:`i` is even
and :math:`(i - 1)\mathrel{/}2` if :math:`i` is odd.
The cost-values of :math:`PG`-edges are not stored anywhere. They are calculated in the D-steps or the maximum calculation in *K**, whenever they are needed.

.. figure:: Sosa-Stradonitz-System.svg
   :align:  center

   The Sosa-Stradonitz indexing

The incoming-heaps of nodes in :math:`G` are built as soon as the nodes are expanded by an A-step and kept up-to-date whenever a new sidetrack-edge is found during an A-step.
The path graph is updated after each A-step in a way that the tree-heaps of all already expanded nodes are built recursively from scratch.
A version number of the tree-heap is stored in each :math:`G`-node-object to make sure that the recursive algorithm doesn't rebuild already completed tree-heaps.

Because there are no pointers between the :math:`PG`-nodes, the methods which use the heaps must include all index computations themselves.
Each object for nodes in :math:`G` holds pointers to its tree- and incoming-heap arrays. These can be used to find the only incoming-heap-child of a :math:`root_{in}`-node,
if the latter is found in a tree-heap:
First, one can look up the target-node of the represented sidetrack-edge and then one can consider the pointer at index 1 of this node's incoming-heap array.
The child-nodes behind cross-edges can be found by looking at the origin of the sidetrack-edge represented by the considered :math:`PG`-nodes.
After a :math:`PG`-node is discovered by a D-step, it doesn't change its position in the path-graph any more (cf. Lemma 7 in [AlLe11] - see `Literature`_).
This is why to store a reference to a :math:`PG`-node, it is sufficient to store the type of the heap that it is included in,
the owning node of that heap and the index of the position in it.

To store :math:`PG`-paths that have been followed during the performance of D-steps, chains of such storage-objects (named *CD_list_obj*-objects in the code) are used.
It is important to note the bijection between :math:`PG`-paths starting at :math:`\mathcal{R}` and sequences of successor-choices in :math:`PG` made by D-steps.
These sequences of successor-choices are used to effectively store :math:`PG`-paths :math:`\sigma` starting at :math:`\mathcal{R}` in the algorithm which of
:math:`\textrm{seq}(\sigma)` and :math:`\chi(\textrm{seq}(\sigma))` is computed later.
In practice, what is stored is not the sequence, but each entry in the open list (and also the closed list) for D-steps represents a successor-choice,
where the corresponding *CD_list_obj*-object contains a predecessor-pointer to a *CD_list_obj*-object on the closed list, which makes it possible to trace back
the complete :math:`PG`-path taken.
The *CD_list_obj*-objects also hold a boolean storing whether the successor is behind a cross-edge or not, which is needed to compute :math:`\textrm{seq}(\sigma)`.

Since in this project we are loading nodes and edges from files and not using databases etc. with the consequence that the edges-file must
be read completely to collect all outgoing edges per node, searching for nodes and edges on-the-fly is even less effective here than complete
preassembling of :math:`G`. This is why we leave out this essential on-the-fly aspect of the original *K** algorithm for the original graph :math:`G`.
