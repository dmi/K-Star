.. K* documentation master file, created by
   sphinx-quickstart on Wed Dec 12 03:31:29 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

K*-Project-Documentation
************************

.. toctree::
   :maxdepth: 2
   
.. include:: motivation.rst

.. include:: basics.rst

.. include:: implementation.rst


The `g` module
--------------

.. automodule:: g
   :members:
   :member-order: bysource

The `pg` module
---------------

.. automodule:: pg
   :members:
   :member-order: bysource

The `a_step` module
-------------------

.. automodule:: a_step
   :members:
   :member-order: bysource

The `d_step` module
-------------------

.. automodule:: d_step
   :members:
   :member-order: bysource

The `ksp` module
----------------

.. automodule:: ksp
   :members:
   :member-order: bysource

The `function_test` module
--------------------------

.. automodule:: function_test
   :members:
   :member-order: bysource

.. include:: tutorial.rst

.. include:: results.rst

.. include:: literature.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
