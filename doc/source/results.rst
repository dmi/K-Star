Results
=======

*K** is a very quick algorithm to solve the *ksp*-problem.
Even on 10 year old machines, if the extension condition is set to exponential mode and the logging mode is set to *logging.ERROR*,
it can find the 1000 shortest paths in the 257-nodes example dataset '*GoeVB*' from the south-west stop "Hagenbreite"
to the very distant north-east stop (31 stops apart) "Eschenbreite" in less than one second.
It is not surprising though that most of the solution paths are just variations of each other with additional cycles or subsets of the most common
small detours that appear time and again.
An interesting question is how much the code-performance could be improved by using a pointer-implementation of the min-heaps instead of
an array implementation. Then the "*cheap copy*" (cf. p. 2135 in [AlLe11] - see `Literature`_)) would be possible but require a certain
amount of copy- and linking-processes as well.
