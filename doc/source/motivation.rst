Motivation
==========

A very frequent problem regarding directed weighted Graphs is to find the shortest path between a start-node and a target-node.
This problem is also called the shortest-path (in short: *sp*) problem. It is solved elegantly by the *Dijkstra-Algorithm* or
- if heuristic estimates for each node's distance to the target-node are given - by its generalization *A**, where the
Dijkstra-algorithm is a special case, if the heuristics :math:`h` is set globally to zero.
Finding the 2nd-shortest, 3rd-shortest ... and finally the :math:`k`-shortest path as well is named the :math:`k`-shortest-path (in short: *ksp*)
problem and appears in many fields of graph-analysis. The most prominent examples are probably navigation systems or line-planning
in public transport, but computation of counterexamples in stochastic model checking as in [AlLe11], p.2152 (see `Literature`_) is a frequent
application as well.

For this problem, which is the fundamental problem of this project, also a lot of solution algorithms exist.
To compare them, let :math:`n` be the number of nodes in the graph and :math:`m` the number of edges.
Since in this project we allow loops in the solution paths, not all *ksp* algorithms are suitable.
The most important ones which match our conditions though are the following:

1. *Eppstein's Algorithm* (in short: *EA*) with the state of the art's worst-case complexity
   of :math:`\mathcal{O}(m + n\log n + k)` in both runtime and space
2. The *lazy variant of Eppstein's algorithm* (in short: *LVEA*) with the same worst-case complexity as EA but better performance
   in practice regarding both runtime and space
3. *K** which also has the worst-case complexity of :math:`\mathcal{O}(m + n\log n + k)` in both runtime and space, but even outperforms
   *LVEA* in practice, where one of the reasons is that an exhaustive presearch-algorithm investigating the whole problem graph is not needed here
   and *K** uses heuristic estimates similar as in *A**.

*LVEA* was presented by Víctor M. Jiménez and Andrés Marzal in 2003 (see [JiMa03] in `Literature`_) while *K** was published by Husain Aljazzar and Stefan Leue in 2011.
*K** is the central topic of this project. It is described in detail in [AlLe11] (see `Literature`_) which is also the main reference of this project
and includes a short discussion of above algorithms as the related work in subsection 1.1.
