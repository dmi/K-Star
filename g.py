"""This module handles the graph construction of the original graph G.

It includes node- and edge-classes.
The `k_star()` function of the `ksp` module holds a nodes-list which
includes all nodes, while each node holds a list of outgoing edges.
`assemble_whole_graph()` is used to create this graph-structure from
given files.

"""

import logging
import os.path



class Cnode:
  """This class represents the nodes of the original graph G.
  
  Attributes
  ----------
  id : int
    Number by which the node can be identified
    
    This is at the same time the list index of the `nodes` list in
    `k_star()` at which this node will be referred to.
  short_name : str
    Short name of the node
  long_name : str
    Descriptive name of the node
  x : float
    `x`-coordinate of the node (might be scaled)
    
    See heuristic function `h()` in the `a_step` module for the usage of
    conversion factors.
  y : float
    `y`-coordinate of the node (might be scaled)
    
    See heuristic function `h()` in the `a_step` module for the usage of
    conversion factors.
  outgoing_edges : list
    List of `Cedge` object pointers describing edges originating at this
    node
  incoming_edges : list
    List of `Cedge` object pointers describing edges arriving at the node
  g : float
    Length of shortest path from the ksp start node to this node
  predecessor : Cnode
    Predecessor of this node in shortest path from ksp start node to this
    node
  closed : bool
    Boolean value set to True, if this node has already been expanded in
    an A-step
  inc_heap : list
    List of `Cpg_node` object pointers forming the incoming heap of this
    node
    
    For more information see `pg` module.
  tree_heap : list
    List of `Cpg_node` object pointers forming the tree heap of this node
    
    For more information see `pg` module.
  tree_heap_version : int
    This is the number of times, that this node's tree heap has been
    (re-) constructed.
    
    For more information see `pg` module.
  
  See Also
  --------
  pg : This module includes the implementation of nodes in PG.
  
  """
  
  def __init__(self, id, short_name, long_name, x, y):
    """The constructor sets up those attributes which are available in a
    stops-file.
    
    Parameters
    ----------
    id : int
      Number by which the node can be identified
      
      This is at the same time the list index of the `nodes` list in
      `k_star()` at which this node will be referred to.
    short_name : str
      Short name of the node
    long_name : str
      Descriptive name of the node
    x : float
      `x`-coordinate of the node (might be scaled)
      
      See heuristic function `h()` in the `a_step` module for the usage of
      conversion factors.
    y : float
      `y`-coordinate of the node (might be scaled)
      
      See heuristic function `h()` in the `a_step` module for the usage of
      conversion factors.
    
    """
    
    self.id                = id
    self.short_name        = short_name
    self.long_name         = long_name
    self.x                 = x
    self.y                 = y
    
    self.outgoing_edges    = []
    self.incoming_edges    = []
    
    self.g                 = None
    self.predecessor       = None
    self.closed            = False
    
    self.inc_heap          = []
    self.tree_heap         = []
    
    self.tree_heap_version = 0
  
  
  def add_outgoing_edge(self, edge):
    """This function adds an edge to the node's list of outgoing edges.
    
    Parameters
    ----------
    edge : Cedge
      The edge in G to be added to the node
    
    """
    
    self.outgoing_edges.append(edge)
    
    logging.info("Added edge '%i':" % edge.id)
    logging.info("--> Edge from '%s' to '%s' with length %f." % (edge.left_stop.long_name, edge.right_stop.long_name, edge.length))


class Cedge:
  """This class represents the directed edges of the original graph G.
  
  It is not used for nodes of the path graph PG. Those are handled in
  the `pg` module.
  
  Attributes
  ----------
  id : int
    Number by which the edge can be identified
    
    Actually this attribute is never used in this project and could be
    removed to save memory. It is just saved for extensibility and printed
    when the edge is added to the graph.
  left_stop : Cnode
    Pointer to the node object of the origin of the edge
  right_stop : Cnode
    Pointer to the node object of the target of the edge
  length : float
    Cost value `c` of the edge
  
  """
  
  def __init__(self, id, left_stop, right_stop, length):
    """The constructor just assigns all attributes.
    
    Parameters
    ----------
    id : int
      Number by which the edge can be identified
    left_stop : Cnode
      Pointer to the node object of the origin of the edge
    right_stop : Cnode
      Pointer to the node object of the target of the edge
    length : float
      Cost value `c` of the edge
    
    """
    
    self.id         = id
    self.left_stop  = left_stop
    self.right_stop = right_stop
    self.length     = length


def assemble_whole_graph(nodes_filename, edges_filename, nodes):
  """This function sets up a list of nodes (that outgoing edges are
  attached to) by a nodes-file and an edges-file and thus creates a whole
  graph.
  
  Parameters
  ----------
  nodes_filename : str
    String containing the file name of the .giv-file containing the nodes
    
    Each line of this file must obey the pattern
    
    stop-id; short-name; long-name; x-coordinate; y-coordinate
    
    to describe exactly one node.
    Lines starting with '#' will be omitted.
    The list must be sorted by the node-ids in ascending order.
  edges_filename : str
    String containing the file name of the .giv-file containing the edges
    
    Each line of this file must obey the pattern
    
    edge-id; left-stop-id; right-stop-id; length
    
    to describe exactly one edge.
    Lines starting with '#' will be omitted.
    This function does not expect the lines to be sorted by left-stop-id
    and does not benefit from it in any way.
  nodes : list
    Quasi empty list to be filled by the function with pointers to
    `Cnode`-objects of all the nodes in G
    
    The list must be initialized as [0], which is a list, that contains
    only a dummy placeholder integer 0. The intention of this is to make
    the index of a node-pointer in this list equal the node's id in the
    nodes-file.
  
  Notes
  -----
  This is obviously not an on-the-fly-method, but since the project uses
  edges-files which must be searched completely to find all outoing edges
  of a node, the purpose of an on-the-fly-method would be undermined
  anyway.
  
  """
  
  
  # Set up nodes:
  
  if os.path.exists(nodes_filename):
    
    file = open(nodes_filename, "r", encoding = "utf-8")
    
    for line in file:                                                   # Gathering the nodes line by line
      
      if line[0] == "#":
        continue
      
      entries = line.strip().split(";")
      
      id         =   int(entries[0])
      short_name =       entries[1]
      long_name  =       entries[2]
      x          = float(entries[3])
      y          = float(entries[4])
      
      node = Cnode(id, short_name, long_name, x, y)
      
      nodes.append(node)
      
      logging.info("Added node '%s'." % long_name)
    
    file.close()
  
  
  # Set up edges:
  
  if os.path.exists(edges_filename):
    
    file = open(edges_filename, "r")
    
    for line in file:                                                   # Gathering the edges line by line
      
      if line[0] == "#":
        continue
      
      entries = line.strip().split(";")
      
      id         =       int(entries[0])
      left_stop  = nodes[int(entries[1])]                               # Note that node-pointers are stored here, created directly by the node-ids.
      right_stop = nodes[int(entries[2])]                               #
      length     =     float(entries[3])
      
      edge = Cedge(id, left_stop, right_stop, length)
      
      left_stop.add_outgoing_edge(edge)
    
    file.close()
  
  return
