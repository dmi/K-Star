"""This module handles the performance of D-steps and the conversion of
PG-paths (starting at PG-root) to G-paths.

Notes
-----
A D-step is reminiscent of a single expansion step of the Dijkstra-
algorithm on PG starting at the PG-root - with the difference that multiple
instances of a node can be on the open list simultaneously as there is no
checking for existing entries with the same PG-node on the open list nor on
the closed list. The closed list for D-steps is only used to memorize all
entries that have already been expanded. This is important for the case
that new PG-nodes appear after an A-step as successors of already closed
PG-nodes. Those new nodes are first put on a refresh-list and later placed
on the open list by `refresh_olD()`.

When the Dijkstra-like D-steps crawl through PG, the options where to go
next (corresponding 1 to 1 to an established PG-path starting at the
G-root) form a tree. Entries (also called 'elements' in this project) of
the open list for D-steps and the respective closed list are pointers to
`CD_list_obj` objects. These objects hold a predecessor pointer which makes
it possible to trace back the PG-path taken all the way back to the PG-root
(which is also the root of said tree). If the predecessors of PG-paths were
stored in `Cpg_node` objects, it would not be possible to have working
cyclic paths. In the said tree on the other hand there are no cycles.

Notation: olD means 'open list D-step' while olA means 'open list A-step'.

"""

import logging



class CD_list_obj:
  """ This class represents the elements of the open list and the closed
  list used in D-steps.
  
  Attributes
  ----------
  pg_node_type : string
    "inc_root", "non_inc_root" or "pg_root" as described below
  heap_owner : Cnode
    The node in G that the considered heap belongs to
  pos : int
    The position in the heap, i.e. the index of the array
  d : float
    The length of the PG-path taken from the PG-root to the considered
    PG-node
  predecessor : CD_list_obj
    The 'CD_list_obj' object which describes the PG-node which is the
    predecessor of the considered PG-node in the PG-path taken
  reached_by_ce : bool
    An indicator whether the last edge of the PG-path taken is a cross-edge
  
  Notes
  -----
  
  A PG-node is uniquely identified by the type of heap it is a part of, the
  node of G that the heap belongs to and the position in the heap.
  Here the type of heap can be "inc_root" if the PG-node is in both an
  incoming-heap and a tree-heap, "non_inc_root" if the PG-node is only in
  an incoming-heap, or "pg_root" if the PG-node is the PG-root.
  
  """
  
  def __init__(self, pg_node_type, heap_owner, pos, d, predecessor, reached_by_ce):
    """The constructor just assigns all attributes.
    
    Parameters
    ----------
    pg_node_type : string
      "inc_root", "non_inc_root" or "pg_root" as described above
    heap_owner : Cnode
      The node in G that the considered heap belongs to
    pos : int
      The position in the heap, i.e. the index of the array
    d : float
      The length of the PG-path taken from the PG-root to the considered
      PG-node
    predecessor : CD_list_obj
      The `CD_list_obj` object which describes the PG-node which is the
      predecessor of the considered PG-node in the PG-path taken
    reached_by_ce : bool
      An indicator whether the last edge of the PG-path taken is a
      cross-edge
    
    """
    
    self.pg_node_type  = pg_node_type           # The following types are possible: "pg_root", "inc_root", "non_inc_root"
    self.heap_owner    = heap_owner
    self.pos           = pos
    self.d             = d
    
    self.predecessor   = predecessor            # Pointer to another D_list_obj or None
    self.reached_by_ce = reached_by_ce          # ce means cross edge and reached_by_ce is set to True, if the Dijkstra algorithm reached the pg_node by a cross edge


def sidetrack_edge_seq(n_element):
  """This function returns the sidetrack-edge sequence of a PG-path
  starting at the PG-root.
  
  Parameters
  ----------
  n_element : CD_list_obj
    The last `CD_list_obj` created by a D-step while creating the PG-path
    which of to get the sidetrack-edge sequence
  
  Returns
  -------
  list
    The sidetrack-edge sequence of the PG-path
    
    If the sequence is not empty, then its order of sidetrack-edges is the
    same as that of the path in G. So to create the path in G, the returned
    sequence must be iterated through starting at the end.
  
  """
  
  # Add the first sidetrack-edge (if any):
  
  n_type = n_element.pg_node_type
  
  if   n_type == "pg_root":
    
    return []                                                   # Path from PG-root to PG-root means no sidetrack-edges at all
  
  elif n_type == "inc_root":
    
    n = n_element.heap_owner.tree_heap[n_element.pos]
    
  else: # Here we have n_type == "non_inc_root"
    
    n = n_element.heap_owner.inc_heap[n_element.pos]
  
  sequence = [n]
  
  
  # Iterate backwards through the CD_list_obj-objects created by the D-steps (i.e. iterate backwards through the PG-path):
  
  predecessor = n_element.predecessor
  
  predecessor_type = predecessor.pg_node_type
  
  while predecessor_type != "pg_root":
    
    if n_element.reached_by_ce:
      
      if predecessor_type == "inc_root":
        
        p = predecessor.heap_owner.tree_heap[predecessor.pos]
        
      else: # Here we have predecessor_type == "non_inc_root"
        
        p = predecessor.heap_owner.inc_heap[predecessor.pos]
      
      sequence.append(p)
      
    n_element = predecessor
    
    predecessor = n_element.predecessor
    
    predecessor_type = predecessor.pg_node_type
  
  return sequence


def print_seq(sequence):
  """This functions prints a sidetrack-edge sequence to the terminal.
  
  Parameters
  ----------
  sequence : list
    The sidetrack-edge sequence to print
  
  """
  
  if len(sequence) == 0:
    
    logging.info("(Empty sequence)")
    
    return
  
  n = sequence[0]
  
  s = "('%s' --> '%s')" % (n.left_stop.short_name, n.right_stop.short_name)
  
  for i in range(1, len(sequence)):
    
    n = sequence[i]
    
    s += "  ...  ('%s' --> '%s')" % (n.left_stop.short_name, n.right_stop.short_name)
  
  logging.info(s)


def chi(sequence, start_node, target_node):
  """This function returns the path in G which belongs to a sidetrack-edge
  sequence.
  
  Parameters
  ----------
  sequence : list
    The sidetrack-edge sequence
  start_node : Cnode
    The start-node of the ksp-problem
  target_node : Cnode
    The target-node of the ksp-problem
  
  Returns
  -------
  list
    The path in G that belongs to the sidetrack-edge sequence
  
  """
  
  pi = []
  
  
  # Include all sidetrack-edges:
  
  node = target_node                            # Starting at the target-node
  
  for edge in reversed(sequence):               # For each sidetrack-edge (iterating backwards)...
    
    right_stop = edge.right_stop
    
    while node != right_stop:                          # Collect the nodes backwards through the shortest path tree until the end of the sidetrack-edge is met 
      
      pi.insert(0, node)
      
      node = node.predecessor
    
    pi.insert(0, right_stop)
    
    node = edge.left_stop
  
  
  # Follow the shortest path tree all the way back to the start-node of the ksp-problem:
  
  while node != start_node:
    
    pi.insert(0, node)
    
    node = node.predecessor
  
  pi.insert(0, start_node)
  
  return pi


def print_path(path, use_logger):
  """This functions prints a path in G to the terminal.
  
  Parameters
  ----------
  path : list
    The path to print
  use_logger : bool
    The type of target for the output
    
    If `use_logger` is True, the path is printed according to the logger
    settings.
    
    If `use_logger` is False, the path is printed to the terminal.
  
  """
  
  s = "'%s'" % path[0].long_name
  
  for i in range(1, len(path)):
    
    s += " --> '%s'" % path[i].long_name
  
  if use_logger:
    logging.info(s)
  else:
    print(s)


def path_length(path):
  """This function returns the length of a path in G.
  
  Parameters
  ----------
  path
    The path which of to calculate the length
  
  Returns
  -------
  float
    The length of the path
  
  """
  
  length = 0
  
  for i in range(0, len(path) - 1):
    
    for edge in path[i].outgoing_edges:         # Find the edge to the next node and add its length
      
      if edge.right_stop == path[i + 1]:
        
        length += edge.length
        
        break
  
  return length


def insert_into_olD(open_list_D, m_type, m_heap_owner, m_pos, m_d, predecessor, reached_by_ce):
  """This function inserts a new entry into the open list for D-steps.
  
  More specifically, a new `CD_list_obj` object is created out of a PG-node
  and inserted into the open list.
  
  The object is inserted in a way that the ascending order of the
  d-attributes is maintained.
  
  Notation: olD means 'open list D-step'.
  
  Parameters
  ----------
  open_list_D : list
    The open list for D-steps
  m_type : string
    "inc_root", "non_inc_root" or "pg_root" as described in the notes of
    `CD_list_obj` description
  m_heap_owner : Cnode
    The node in G that the considered heap belongs to
  m_pos : int
    The position in the heap, i.e. the index of the array
  m_d : float
    The length of the PG-path taken from the PG-root to the considered
    PG-node
  predecessor : CD_list_obj
    The `CD_list_obj` object which describes the PG-node which is the
    predecessor of the considered PG-node in the PG-path taken
  reached_by_ce : bool
    An indicator whether the last edge of the PG-path taken is a cross-edge
  
  """
  
  # Find position in open_list_D to insert:
  
  insert_index = 0
  
  for element in open_list_D:
    
    if element.d < m_d:
      insert_index += 1
    else:
      break
  
  
  # Insert node into open_list_D:
  
  open_list_D.insert(insert_index, CD_list_obj(m_type, m_heap_owner, m_pos, m_d, predecessor, reached_by_ce))


def refresh_olD(refresh_list, open_list_D, closed_list_D, target_node):
  """This function adds new successors of already closed PG-nodes to the
  open list for D-steps, after the path graph has been extended.
  
  Parameters
  ----------
  refresh_list : list
    A list that contains all new PG-nodes from the last A-step
  open_list_D : list
    The open list for D-steps
  closed_list_D : list
    The closed list for D-steps
  target_node : Cnode
    The target-node of the ksp-problem
  
  Notes
  -----
  The `refresh-list` must contain all new PG-nodes from the last A-step and
  is cleared at the end of this function.
  
  """
  
  for pg_node in refresh_list:
    
    for n_element in closed_list_D:
      
      # For each successor m (in PG) of the closed PG-node n, check if at the position of m in the path graph the new object pg_node (from the refresh-list) has appeared.
      # And if so, add the according entry to the open list:
      
      n_type       = n_element.pg_node_type
      n_heap_owner = n_element.heap_owner
      n_pos        = n_element.pos
      n_d          = n_element.d
      
      if n_type == "pg_root":
        
        heap = target_node.tree_heap
        
        if len(heap) >= 2:
          
          m = heap[1]
          
          if m == pg_node:
            
            insert_into_olD(open_list_D, "inc_root", target_node, 1, m.delta, n_element, True)
      
      else:
        
        if n_type == "inc_root":
          
          heap      = n_heap_owner.tree_heap
          n         = heap[n_pos]
          
          right_stop = n.right_stop
          i_heap     = right_stop.inc_heap                      # Incoming-heap that n is a part of
          
          if len(i_heap) >= 2:
            
            m = i_heap[1]
            
            if m == pg_node:
              
              insert_into_olD(open_list_D, "non_inc_root", right_stop, 1, n_d + m.delta - n.delta, n_element, False)
          
          insert_type = "inc_root"
          
        else: # Here we have n_type == "non_inc_root"
          
          heap      = n_heap_owner.inc_heap
          n         = heap[n_pos]
          
          insert_type = "non_inc_root"
        
        left_stop = n.left_stop
        c_heap    = left_stop.tree_heap                 # Target tree-heap of outgoing cross edge
        
        if len(c_heap) >= 2:
          
          m = c_heap[1]
          
          if m == pg_node:
            
            insert_into_olD(open_list_D, "inc_root", left_stop, 1, n_d + m.delta, n_element, True)
        
        length = len(heap)
        
        if length >= 2 * n_pos + 1:                     # Check child_a
          
          m = heap[2 * n_pos]
          
          if m == pg_node:
            
            insert_into_olD(open_list_D, insert_type, n_heap_owner, 2 * n_pos, n_d + m.delta - n.delta, n_element, False)
          
          if length >= 2 * n_pos + 2:                           # Check child_b
            
            m = heap[2 * n_pos + 1]
            
            if m == pg_node:
              
              insert_into_olD(open_list_D, insert_type, n_heap_owner, 2 * n_pos + 1, n_d + m.delta - n.delta, n_element, False)
  
  refresh_list.clear()          # Prevent registering new nodes in the open list twice


def perform_D_step(open_list_D, closed_list_D, start_node, target_node, path_list):
  """This function performs a D-step.
  
  Parameters
  ----------
  open_list_D : list
    The open list for D-steps
  closed_list_D : list
    The closed list for D-steps
  start_node : Cnode
    The start-node of the ksp-problem
  target_node : Cnode
    The target-node of the ksp-problem
  path_list : list
    The list of current solution paths for the ksp-problem
  
  """
  
  # Pop first element of olD:
  
  n_element    = open_list_D.pop(0)
  
  n_type       = n_element.pg_node_type
  n_heap_owner = n_element.heap_owner
  n_pos        = n_element.pos
  n_d          = n_element.d
  
  
  # Put successors on olD and attach parent link to them referring to n_element
  
  if n_type == "pg_root":
    
    heap = target_node.tree_heap
    
    if len(heap) >= 2:
      
      m = heap[1]
      
      insert_into_olD(open_list_D, "inc_root", target_node, 1, m.delta, n_element, True)
  
  else:
    
    if n_type == "inc_root":
      
      heap      = n_heap_owner.tree_heap
      n         = heap[n_pos]
      
      right_stop = n.right_stop
      i_heap     = right_stop.inc_heap                  # Incoming-heap that n is a part of
      
      if len(i_heap) >= 2:
        
        m = i_heap[1]
        
        insert_into_olD(open_list_D, "non_inc_root", right_stop, 1, n_d + m.delta - n.delta, n_element, False)
      
      insert_type = "inc_root"
      
    else: # Here we have n_type == "non_inc_root"
      
      heap      = n_heap_owner.inc_heap
      n         = heap[n_pos]
      
      insert_type = "non_inc_root"
      
    left_stop = n.left_stop
    c_heap    = left_stop.tree_heap             # Target tree-heap of outgoing cross edge
    
    if len(c_heap) >= 2:
      
      m = c_heap[1]
      
      insert_into_olD(open_list_D, "inc_root", left_stop, 1, n_d + m.delta, n_element, True)
    
    length = len(heap)
    
    if length >= 2 * n_pos + 1:                 # Check child_a
      
      m = heap[2 * n_pos]
      
      insert_into_olD(open_list_D, insert_type, n_heap_owner, 2 * n_pos, n_d + m.delta - n.delta, n_element, False)
      
      if length >= 2 * n_pos + 2:                       # Check child_b
        
        m = heap[2 * n_pos + 1]
        
        insert_into_olD(open_list_D, insert_type, n_heap_owner, 2 * n_pos + 1, n_d + m.delta - n.delta, n_element, False)
  
  
  # Put list-element on closed_list_D:
  
  closed_list_D.append(n_element)
  
  
  # Add path to path_list:
  
  sequence = sidetrack_edge_seq(n_element)              # Get the sidetrack edge sequence
  
  logging.info("\nSidetrack edge sequence:")
  print_seq(sequence)
  
  pi       = chi(sequence, start_node, target_node)     # Build the path pi out of the sidetrack edge sequence
  
  logging.info("\nPath found:")
  print_path(pi, True)
  logging.info("")
  
  path_list.append(pi)                                  # Add the path to the list of current solution paths
