"""This module handles the graph construction of the path graph PG.

Notes
-----
The path graph consists of nodes which are all part of incoming-heaps and
some additionally of tree-heaps. These heaps are binary min-heaps
represented by simple arrays starting at index 0 (incoming-heaps) or
index 1 (tree-heaps). The incoming-heaps have a root node at index 0 which
has as an exception at most one child. All other nodes can have up to two
child-nodes. It is important to note, that an array itself does not store
any pointers to children or parents. Here we use the Sosa-Stradonitz-System
to find the first child of a node with index :math:`i` at index
:math:`2\mathrel{\cdot}i` (if available) and its right child at index
:math:`2\mathrel{\cdot}i + 1` (if available).
That way a parent of a node with index :math:`i` can only be at index
:math:`i\mathrel{/}2`, if :math:`i` is even, or at index
:math:`(i - 1)\mathrel{/}2`, if :math:`i` is odd. Since the binary
min-heaps are filled starting at the root node, all nodes have parents
except that one with index 0 for incoming-heaps and that with index 1 for
tree-heaps. All algorithms that operate on these heaps must include the
small calculations to find child- and parent-nodes.
They are part of the `D-step` or `ksp` module or implemented below.

For each node v of the original graph G the incoming-heap of v is created
as soon as v is expanded in an A-step. This is accomplished by
`build_incoming_heap()` which itself uses `update_incoming_heap()` to add
all sidetrack-edges to the initially empty incoming-heap of v. Later when
new incoming edges (which must be sidetrack-edges then) are found, they're
also added by `update_incoming_heap()`.

After each A-step `reconstruct_tree_heaps()` is invoked, which recursively
rebuilds all tree-heaps using `update_tree_heap()`, where a tree-heap of a
node v is already built, iff the node's `tree_heap_version` attribute
equals the number of performed A-steps (`no_of_extensions`) at that time.

"""

import logging



class Cpg_node:
  """This class represents the nodes of the path graph PG.
  
  Attributes
  ----------
  left_stop : Cnode
    Pointer to `Cnode` object of the origin of the sidetrack-edge that this
    pg_node is representing
  right_stop : Cnode
    Pointer to `Cnode` object of the target of the sidetrack-edge that this
    pg_node is representing
  delta : float
    The detour cost `delta` of the sidetrack-edge that this pg_node is
    representing
  
  """
  
  def __init__(self, left_stop, right_stop, delta):
    """The constructor just assigns all attributes.
    
    Parameters
    ----------
    left_stop : Cnode
      Pointer to `Cnode` object of the origin of the sidetrack-edge that
      this pg_node is representing
    right_stop : Cnode
      Pointer to `Cnode` object of the target of the sidetrack-edge that
      this pg_node is representing
    delta : float
      The detour cost `delta` of the sidetrack-edge that this pg_node is
      representing
    
    """
    
    self.left_stop      = left_stop
    self.right_stop     = right_stop
    self.delta          = delta


def update_incoming_heap(node, edge):
  """This function inserts a sidetrack-edge into the incoming-heap of a
  node.
  
  Parameters
  ----------
  node : Cnode
    The node which incoming-heap the sidetrack-edge is inserted into
  edge : Cedge
    The sidetrack-edge to insert
  
  """
  
  logging.info(" Updating incoming-heap for node %s" % node.long_name)
  
  inc_heap = node.inc_heap
  
  
  # Create the pg_node:
  
  left_stop = edge.left_stop
  
  delta = left_stop.g + edge.length - node.g
  
  pg_node = Cpg_node(left_stop, node, delta)
  
  
  # Place the pg_node at the end of the incoming-heap:
  
  heap_size = len(inc_heap)
  
  if heap_size == 0:                    # Is the incoming-heap empty?
    
    inc_heap.append(pg_node)                    # Put the new node into the empty incoming-heap
    
    return pg_node
  
  inc_heap.append(pg_node)              # Attach the new node to the end of the incoming-heap
  
  current_pos = heap_size 
  
  
  # Get the parent nodes:
  
  if current_pos % 2 == 0:
    
    parent = inc_heap[current_pos // 2]
    
  else:
    
    parent = inc_heap[(current_pos - 1) // 2]
  
  
  # Heapify-up, if necessary:
  
  while parent.delta > delta:
    
    if current_pos == 1:
      
      # Swap pg_node and its parent in the list:
      
      high_pos_node         = inc_heap[0]
      inc_heap[0]           = pg_node
      inc_heap[1]           = high_pos_node
      
      break
      
    elif current_pos in [2, 3]:
      
      # Caculate positions:
      
      parent_pos = 1
      
      next_parent = inc_heap[0]
      
      
      # Swap pg_node and its parent in the list:
      
      high_pos_node         = inc_heap[1]
      inc_heap[1]           = pg_node
      inc_heap[current_pos] = high_pos_node
      
    elif current_pos >= 4:
      
      # Caculate positions:
      
      if current_pos % 2 == 0:
        
        parent_pos = current_pos // 2
        
      else:
        
        parent_pos = (current_pos - 1) // 2
      
      if parent_pos % 2 == 0:
        
        next_parent = inc_heap[parent_pos // 2]
        
      else:
        
        next_parent = inc_heap[(parent_pos - 1) // 2]
      
      
      # Swap pg_node and its parent in the list:
      
      high_pos_node         = inc_heap[parent_pos]
      inc_heap[parent_pos]  = pg_node
      inc_heap[current_pos] = high_pos_node
    
    current_pos = parent_pos
    parent      = next_parent
  
  return pg_node


def build_incoming_heap(node):
  """This function creates the incoming-heap of a node v and is called when
  v is expanded in an A-step.
  
  Parameters
  ----------
  node : Cnode
    The node which incoming-heap is to be created
  
  """
  
  logging.info(" Building incoming-heap of node %s" % node.long_name)
  
  for edge in node.incoming_edges:
    
    if edge.left_stop == node.predecessor:         # Is it the tree-edge?
      continue
    
    update_incoming_heap(node, edge)


def print_incoming_heap(node):
  """This function prints the incoming-heap of a node to the terminal.
  
  The heap is printed in form of a right-aligned table, where 'Position'
  equals the array index of a tree-node and 'From' is the name of the node,
  that the corresponding edge of the tree-node is originating from.
  
  Parameters
  ----------
  node : Cnode
    The node which incoming-heap is to be printed
  
  """
  
  inc_heap = node.inc_heap
  
  logging.info("")
  logging.info("  Incoming-Heap of %s:" % node.long_name)
  logging.info("")
  logging.info("    Position   From                              delta")
  logging.info("    ----------------------------------------------------------")
  
  position = 0
  
  for pg_node in inc_heap:
    
    logging.info("    %8i   %33s %13.3f" % (position, pg_node.left_stop.long_name, pg_node.delta))        
    
    position += 1
  
  logging.info("")


def update_tree_heap(start_node, node, no_of_extensions):
  """This function builds the current version of the tree-heap of a node.
  
  Parameters
  ----------
  start_node : Cnode
    The start-node of the ksp-problem
  node : Cnode
    The node which tree-heap is to be built
  no_of_extensions : int
    The number of A-steps that have already been performed
    
    This can be regarded as the version number of the tree-heap to be built.
  
  """
  
  if node.tree_heap_version == no_of_extensions:        # If the tree-heap is already up-to-date...
    return                                                      # Don't change anything and end this function
  
  tree_heap = node.tree_heap
  
  
  # Clear tree-heap:
  
  tree_heap.clear()
  
  
  # Copy tree-heap of predecessor:
  
  tree_heap.append(0)                                   # Dummy element to fill the first entry
  
  if node != start_node:
    
    predecessor = node.predecessor
    
    update_tree_heap(start_node, predecessor, no_of_extensions)
    
    for pg_node in predecessor.tree_heap[1:]:
      tree_heap.append(pg_node)
  
  
  # Insert root_inc:
  
  if len(node.inc_heap) != 0:
    
    root_inc  = node.inc_heap[0]
    
    
    # Place root_inc at the end of the tree-heap:
    
    heap_size = len(tree_heap)
    
    tree_heap.append(root_inc)                                  # Attach the new node to the end of the incoming-heap
    
    if heap_size > 1:
      
      current_pos = heap_size
    
    
      # Get the parent nodes:
    
      if current_pos % 2 == 0:
        
        parent = tree_heap[current_pos // 2]
        
      else:
        
        parent = tree_heap[(current_pos - 1) // 2]
        
        
      # Heapify-up, if necessary:
      
      delta = root_inc.delta
      
      while parent.delta > delta:
        
        if current_pos in [2, 3]:
          
          # Swap inc_root and its parent in the list:
          
          high_pos_node          = tree_heap[1]
          tree_heap[1]           = root_inc
          tree_heap[current_pos] = high_pos_node
          
          break
        
        else:           # If current_pos >= 4...
          
          # Caculate positions:
          
          if current_pos % 2 == 0:
            
            parent_pos = current_pos // 2
            
          else:
            
            parent_pos = (current_pos - 1) // 2
          
          if parent_pos % 2 == 0:
            
            next_parent = tree_heap[parent_pos // 2]
            
          else:
            
            next_parent = tree_heap[(parent_pos - 1) // 2]
          
          
          # Swap inc_root and its parent in the list:
          
          high_pos_node          = tree_heap[parent_pos]
          tree_heap[parent_pos]  = root_inc
          tree_heap[current_pos] = high_pos_node
        
        current_pos = parent_pos
        parent      = next_parent
  
  node.tree_heap_version = no_of_extensions


def print_tree_heap(node, version_to_name):
  """This function prints the tree-heap of a node to the terminal.
  
  The heap is printed in form of a right-aligned table, where 'Position'
  equals the array index of a tree-node and 'From' or 'To' resp. is the
  name of the node, that the corresponding edge of the tree-node is
  originating from or that it is targeting resp..
  
  Parameters
  ----------
  node : Cnode
    The node which tree-heap is to be printed
  version_to_name : int
    The version which is to be named at the head-line of the table
    
    Since for each node only the current tree-heap is saved and no version
    numer is attached to it, the current no_of_extensions must be passed as
    an argument.
  
  """
  
  tree_heap = node.tree_heap
  
  logging.info("")
  logging.info(" Tree-Heap of %s (PG-Version %i):" % (node.long_name, version_to_name))
  logging.info("")
  logging.info("   Position   From                              To                                delta")
  logging.info("   --------------------------------------------------------------------------------------------")
  
  position = 1
  
  for pg_node in tree_heap[1:]:
    
    logging.info("   %8i   %33s %33s %13.3f" % (position, pg_node.left_stop.long_name, pg_node.right_stop.long_name, pg_node.delta))        
    
    position += 1
  
  logging.info("")


def reconstruct_tree_heaps(closed_list_A, start_node, no_of_extensions):
  """This function builds the current versions of the tree-heaps of all
  nodes in `closed_list_A`.
  
  Parameters
  ----------
  closed_list_A : list
    The list of all nodes that have already been expanded during A-steps
  start_node : Cnode
    The start-node of the ksp-problem
  no_of_extensions : int
    The number of A-steps that have already been performed
    
    This can be regarded as the version number of the tree-heaps to be
    built.
  
  """
  
  for node in closed_list_A:
    
    update_tree_heap(start_node, node, no_of_extensions)
    print_tree_heap(             node, no_of_extensions)
